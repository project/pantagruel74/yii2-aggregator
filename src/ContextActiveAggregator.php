<?php

namespace Pantagruel74\Yii2Aggregator;

use Webmozart\Assert\Assert;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;
use yii\db\QueryInterface;

/**
 * @template T
 * @ActiveAggregator<T>
 * @extends ActiveAggregatorTrait<T>
 */
abstract class ContextActiveAggregator extends Model
{
    use ActiveAggregatorTrait;

    public string $context = 'default';

    /**
     * @param string $context
     * @return ActiveQueryInterface
     */
    abstract public function getContext(string $context): ActiveQueryInterface;

    /**
     * @return void
     */
    public function init(): void
    {
        $contextQuery = $this->getContext($this->context);
        Assert::notNull($contextQuery);
        $this->baseQuery = $contextQuery;
        parent::init();
    }
}