<?php

namespace Pantagruel74\Yii2Aggregator;

use Webmozart\Assert\Assert;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

/**
 * @template T
 * @ActiveAggregator<T>
 */
trait ActiveAggregatorTrait
{
    protected ActiveQueryInterface $baseQuery;

    /**
     * @return array<T|ActiveRecordInterface>
     */
    public function getAll(): array
    {
        return $this->baseQuery
            ->all();
    }

    /**
     * @param bool|int $pagination
     * @return ActiveDataProvider
     */
    public function getDataProvider($pagination = false): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->baseQuery,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @return T|ActiveRecordInterface
     */
    public function getOne(): ActiveRecordInterface
    {
        $hmail = $this->baseQuery
            ->one();
        Assert::notNull($hmail);
        Assert::subclassOf($hmail, ActiveRecordInterface::class);
        return $hmail;
    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        return $this->baseQuery;
    }
}