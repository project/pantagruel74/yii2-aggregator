<?php

namespace Pantagruel74\Yii2AggregatorTestUnit;

use Pantagruel74\Yii2TestApp\TestRequest;
use Pantagruel74\Yii2TestApp\YiiDefaultConfig;
use Pantagruel74\Yii2TestAppTestHelpers\AbstractBaseTest;
use yii2tech\filedb\ActiveRecord;
use yii2tech\filedb\Connection;

class ActiveAggregatorGetAllTest extends AbstractBaseTest
{
    /**
     * @return array
     */
    protected function getConfig(): array
    {
        $ds = DIRECTORY_SEPARATOR;
        $config = YiiDefaultConfig::getBase(__DIR__);
        $config['components']['filedb'] = [
            'class' => Connection::class,
            'path' => __DIR__ . $ds . 'tmp',
        ];
        $config['components']['request'] = [
            'class' => TestRequest::class,
            'route' => '',
        ];
        return $config;
    }

    /**
     * @return void
     */
    protected function testScenario(): void
    {
        $ds = DIRECTORY_SEPARATOR;
        $records = (new TestAggregator())->getAll();
        $this->assertEquals(
            include __DIR__ . $ds . 'tmp' . $ds . 'test-ar.php',
            array_map(fn(ActiveRecord $r) => ($r->getAttributes()), $records)
        );

        $records = (new TestAggregator())->getQuery()
            ->andWhere(['count' => 3])
            ->all();
        $this->assertEquals([
            [
                'uuid' => '67005aec-6ff3-48d8-b5fa-5fbf01492fab',
                'count' => 3,
            ],
        ], array_map(fn(ActiveRecord $r) => ($r->getAttributes()), $records));

        $records = (new TestAggregator())->getDataProvider()->getModels();
        $this->assertEquals(
            include __DIR__ . $ds . 'tmp' . $ds . 'test-ar.php',
            array_map(fn(ActiveRecord $r) => ($r->getAttributes()), $records)
        );
    }
}