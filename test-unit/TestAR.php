<?php

namespace Pantagruel74\Yii2AggregatorTestUnit;

use Pantagruel74\Yii2FileActiveRecord\FileActiveRecord;

/**
 * @property string $uuid
 * @property int $count
 */
class TestAR extends FileActiveRecord
{
    public static function fileName(): string
    {
        return 'test-ar';
    }

    public function attributes(): array
    {
        return ['uuid', 'count'];
    }

    public static function primaryKey(): array
    {
        return ['uuid'];
    }
}