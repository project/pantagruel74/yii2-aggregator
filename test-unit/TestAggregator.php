<?php

namespace Pantagruel74\Yii2AggregatorTestUnit;

use Pantagruel74\Yii2Aggregator\ContextActiveAggregator;
use Webmozart\Assert\Assert;
use yii\db\ActiveQueryInterface;

class TestAggregator extends ContextActiveAggregator
{
    /**
     * @param string $context
     * @return ActiveQueryInterface
     */
    public function getContext(string $context): ActiveQueryInterface
    {
        $contexts = [
            'default' => TestAR::find(),
        ];
        Assert::notNull($contexts[$context]);
        return $contexts[$context];
    }
}